% Elaina Stuckey & Wuzhou Zu
% Engr 1410-627 Team 627C
% Purpose: Take in data, plot it, and determine the values of n, q, and h

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Variable List:
%   col - markers and colours for graphs
%   colour - colours for lines
%   count/dataSet - counter variables
%   anything with the word size - size of an array
%   sphereInfoData - data from the sphere info sheet
%   sphereInfoHeaders - headers from the sphere info sheet
%   sphereDataData - data from the sphere data sheet
%   cylinderInfoData - data from the cylinder info sheet
%   cylinderInfoHeaders - headers from the cylinder info sheet
%   cylinderDataData - data from the cylinder data sheet
%   table1Data/table1Headers - table 1
%   table2Data/table2Headers - table 2
%   equations - r values for each data set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Just pass in all of the data you have because what is this efficiency you speak of?
function [powerArea, powerMCp, stdh] = F_627C(setNum, sphereInfoData, sphereInfoHeaders, sphereDataData, ...
    cylinderInfoData, cylinderInfoHeaders, cylinderDataData, table1Data, ...
    table1Headers, table2Data, table2Headers)

%% All of the constants and declarations
    % Arrays of colours that can be used for the plotting of things
    col = {'ok', 'xb', 'dc', 'hg', 'sm', 'dr', 'hb', 'rp', 'sb', 'rs'};
    colour = {'k', 'b', 'c', 'g', 'm', 'r', 'b', 'r', 'b', 'r'};
    
    % Counter Variables- keep the correct place in data & parallel arrays
    count = 1;
    dataSet = 1;
    
    % Make a new figure for the main graphs and make it decently big
    % because there will be 4 subplots
    figure('color', 'w','Position', [0, 50, 2000, 950]);
    
    % Add the aluminum data to the cylinders data by finding it first
    sizeTest = size(cylinderDataData);
    findMe = find(strcmp(sphereInfoHeaders(:,2), table2Headers(2,1)));
    cylinderDataData(sizeTest(1)+1:sizeTest(1)+2, :) = sphereDataData(findMe:findMe+1,:);
    cylinderDataData(sizeTest(1)+1:sizeTest(1)+2,1) = (sizeTest(1)+2)/2;
    sizeTest = size(cylinderInfoHeaders);
    cylinderInfoHeaders(sizeTest(1)+1,2) = table2Headers(2,1);
    sizeTest = size(cylinderInfoData);
    cylinderInfoData(sizeTest(1)+1,sizeTest(2)-1) = sphereInfoData(findMe(1),sizeTest(2)-1);
    

    %% Display all of the data
    % for each data set the for loop runs
    for numSet = 1:(setNum*2)
        % Create a subplot for each data set and a linear and normal graph
        subplot(setNum, 2, numSet);
        
        % The first data set is spheres
        if dataSet == 1
            Data = sphereDataData;
            Info = sphereInfoData;
            Headers = sphereInfoHeaders;
            datSize = size(Data);
            Shape = 'Sphere';
        
        % The second is cylinders
        else
            Data = cylinderDataData;
            Info = cylinderInfoData;
            Headers = cylinderInfoHeaders;
            datSize = size(Data);
            Shape = 'Cylinder';
            
        end
        
        % Plot all of the points both rectilinearly and on a semi-log plot
        for rows = 1:2:datSize(1)
            %disp(Data(rows, 1));
            if mod(numSet,2) == 0
                semilogy(Data(rows,3:datSize(2)), Data(rows+1,3:datSize(2))- Info(Data(rows, 1),6), col{count},'MarkerFaceColor', colour{count});
                Type = 'Log Y';
            else
                %disp(Data(rows,3:datSize(2)))
                plot(Data(rows,3:datSize(2)), Data(rows+1,3:datSize(2))- Info(Data(rows, 1),6), col{count},'MarkerFaceColor', colour{count});
                Type = 'Rectilinear';
            end

            hold on;
            
            % Polyfit the data and then plot the trendlines
            C = polyfit(Data(rows,3:datSize(2)), log(Data(rows+1,3:datSize(2)) - Info(Data(rows, 1),6)), 1);
            
            % The data for spheres is in cols 1 and 2; cylinders in 3 and 4
            if dataSet == 1
                equations(count, 1) = C(1);
                equations(count, 2) = exp(C(2));
            else
                equations(count, 3) = C(1);
                equations(count, 4) = exp(C(2));
            end

            tth = Data(rows,3):Data(rows,datSize(2));
            
            % Since all of the eqs are in one array, we need to know what
            % dataset we're on to make sure we plot the right data
            if mod(numSet,2) == 0
                if dataSet == 1
                    x = 1;
                    y = 2;
                else
                    x = 3;
                    y = 4;
                end
                semilogy(tth, equations(count,y) * exp(equations(count,x) * tth), colour{count}, 'LineWidth', 2);
            else
                if dataSet == 1
                    x = 1;
                    y = 2;
                else
                    x = 3;
                    y = 4;
                end
                plot(tth, equations(count,y) * exp(equations(count,x) * tth), colour{count}, 'LineWidth', 2);
            end
            
            r{rows,1} = sprintf('%s', (Headers{Data(rows,1)+1,2}));
            r{rows+1,1} = sprintf('%s', (Headers{Data(rows,1)+1,2}));
            
            
            count = count + 1;


        end
        
        %Print the correct equations
        if dataSet == 1
            lol = sprintf('%0.3f', equations(1,1));
            om = sprintf('DeltaT = %0.0f*e^{(%d t)}', equations(1,2),equations(1,1));
            text(0.5,20, om, 'BackgroundColor', 'w', 'EdgeColor', colour{1});
        else
            om = sprintf('DeltaT = %0.0f*e^{(%d t)}', equations(1,4),equations(1,3));
            text(0.5,20, om, 'BackgroundColor', 'w', 'EdgeColor', colour{1});
        end
        
        % The fancy plotting things
        rSiz = size(r);
        legend(r(1:rSiz,:), 'Location', 'BestOutside');
        xlabel('Time (t) [min]');
        ylabel('Temperature Decline (Delta T) [Degrees C]');
        grid on;
        titleLine = sprintf('Comparison of Cooling Times for %ss "%s"', Shape, Type);
    
        title(titleLine);
        
        % If we've run through twice, go to the second data set
        if mod(numSet,2) == 0
            dataSet = dataSet + 1;
        end
        
        % reset the counter variable
        count = 1;
    end
    
hold off;
%% Find all of the other things. Yay.
%% More Constants and Such
    % New figure
    figure('color', 'w','Position', [0, 50, 500, 500]);
    
    % Size things correctly and make the variables into the variables they
    % should be because these were originally two different functions
    eqSz = size(equations);
    EquationsSp = equations(:,1:2);
    EquationsCyl = equations(:, 3:4);
    Info1 = sphereInfoData;
    Headers1 = sphereInfoHeaders;
    Info2 = cylinderInfoData;
    Headers2 = cylinderInfoHeaders;
    Table1 = table2Data;
    Table2 = table1Data;
    TableHeaders = table1Headers;

    % Conversions, sizes, and declarations of arrays
    ipm = .0254;
    datSize = size(Info1);
    radius = [];
    area = [];
    
%% Find H, 
    for row = 1:datSize(1)
            % Find the correct data since it may not be in order here 
            % Surface area of cylinders
            findMe = find(strcmp(TableHeaders(:,1), Headers1(row+1,2)));
            area(row, 2) = Info1(row, 5) * Table2(findMe-1,2);
            
            % mCp of spheres
            radius(row, 1) = (Table1(row, 1)/2) * ipm;
            radius(row, 2) = (Table1(row, 2)*ipm);
            area(row, 1) = (2 * pi * radius(row, 1)^2) + (2 * pi * radius(row, 1).*radius(row, 2));
    end
         % Account for the fact we added aluminum so that the code doesn't
         % completely break itself here
         arSize = size(area);
         findMe = find(strcmp(sphereInfoHeaders(:,2), table2Headers(2,1)));
         rad = (sphereInfoData(findMe,4) / 2)*ipm;
         surf = ((rad)^2)*(4)*pi;
         area(arSize(1) + 1, 1) = surf;
         area(arSize(1) + 1, 2) = area(findMe,2);
         EquationsSp(eqSz(1),1:2) = EquationsSp(findMe,1:2);
         sie = size(EquationsSp);
         
       % Plot and polyfit all of the things yay
        r = abs(EquationsCyl(:,1)/60);
        
        subplot(2, 1, 1);
       
        loglog(area(:,1), r, 'ob');
        
        hold on;
        
        C1 = polyfit(log10(area(:,1)), log10(r),1);
        m1 = C1(1);
        b1 = 10^(C1(2));
        
        hold on;
        
        small = min(area(:,1));
        big = max(area(:,1));
        vth = small:0.00005:big;
        
        loglog(vth, b1*(vth.^m1),'b');
        
        eq = sprintf('r = %0.3f*A^{%0.3f}', b1, m1);
        
        hold on;
        
        % Casually using legend to place the polyfit in a nice place
        legend(eq, 'Location', 'Best');
        xlabel('Surface Area (A) [m^2]');
        ylabel('Cooling Factor (r) [1/min]');
        title('Cooling Factor v. Surface Area');
        grid on;
        
        %% Second Plot for mCp
        subplot(2, 1, 2);  
        
        % More plotting and polyfit
        loglog(area(:,2), abs(EquationsSp(:,1)/60), 'or', 'MarkerFaceColor', 'r');
        
        C2 = polyfit(log10(area(:,2)), log10(abs(EquationsSp(:,1)/60)),1);
        m2 = C2(1);
        b2 = 10^(C2(2));
        
        hold on;
        
        small = min(area(:,2));
        big = max(area(:,2));
        vth = small:0.005:big;
        
        loglog(vth, b2*(vth.^m2),'-r');
        eq = sprintf('r = %0.3f*A^{%0.3f}', b2, m2);
        
        hold on;
        
        % Casually using legend to place the polyfit in a nice place
        legend(eq, 'Location', 'Best');
        xlabel('Mass * Specific Heat (mCp) [J/K]');
        ylabel('Cooling Factor (r) [1/min]');
        title('Cooling Factor v. Mass * Specific Heat');
        grid on;
    
    %% Heat Transfer Coefficienct
    
    % Round the powers to make pretty units
    powerArea = round(m1);
    powerMCp = round(m2);
    h = [];
    
    % Calculate h for all of the values using the correct shape, material,
    % surface area, and mass
    
    for numSet = 1:setNum*2
      if setNum == 1
          datSize = size(Info1);
          findMe = find(strcmp(sphereInfoHeaders(:,2), table2Headers(2,1)));
         
          rad = (sphereInfoData(findMe,4) / 2)*ipm;
          surf = ((rad)^2)*(4)*pi;
      else
          datSize = size(Info2);
          
          Cp = table1Data(1,2);
          m = sphereInfoData(2,5);
      end
        for rows = 1:datSize(1)
            % use cp for al for cyl and sa for spheres
            if setNum == 1
                h(rows,1) = (abs(EquationsSp(rows,1)/60))/((area(rows,2)^(powerMCp))*(surf^(powerArea)));
            else
                sizeH = size(h);
                h(sizeH(1)+1,1) = (abs(EquationsSp(rows,1)/60))/(((m*Cp)^(powerMCp))*(area(rows,1)^(powerArea)));
            end
            
        end
    end
    
    % Some data analysis here
     Avg = mean(h);
     med = median(h);
     stdv = std(h);
     stdh = mean(h);
     outputs = sprintf('Heat Transfer Coefficient: \nMean = %0.3f [W / (m� K)]\nMedian = %0.3f [W / (m� K)]\nStandard Deviation = %0.3f [W / (m� K)]\n\n', Avg, med, stdv);
     % Display the things. 
     h = msgbox(outputs);
end