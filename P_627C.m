% Elaina Stuckey & Wuzhou Zu
% Engr 1410-627 Team 627C
% Purpose: Take in a series of inputs from a user to determine the cooling
% time or change in temperature for a cooling object.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Variable List:
%   A - surface area [m^2]
%   answer - input values from dialog boxes
%   coolFactor - equivalent of h (heat transfer coefficient [[W / (m^2 K)]
%   data variables (a collection of variables) - data from excel sheets
%   DesiredVal - value the user wants (cooling time or temperature)
%   ft - final temperature [degrees C]
%   it - initial temperature [degrees C]
%   rt - room temperature [degrees C]
%   mass - mass of material [g]
%   Mtr - material
%   MtrSG - specific gravity of material
%   MtrSH - specific heat of material
%   n - power of area term
%   outputDialog - final output
%   q - power mCp
%   r - cooling factor [1/min]
%   radi - radius of cylinder or sphere
%   shape - shape of object
%   V - volume of shape
%   whichWay - choice of first menu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Close and clear all of the things
clear
clc
close all

%% Read in Excel file
[sphereInfoData, sphereInfoHeaders] = xlsread('Project_627C.xlsx','Sphere Information');
[sphereDataData] = xlsread('Project_627C.xslx','Sphere Data');
[cylinderInfoData, cylinderInfoHeaders] = xlsread('Project_627C.xlsx', 'Cylinder Information');
[cylinderDataData] = xlsread('Project_627C.xlsx', 'Cylinder Data');
[table1Data, table1Headers] = xlsread('Project_627C.xlsx', 'Table 1');
[table2Data, table2Headers] = xlsread('Project_627C.xlsx', 'Table 2');

%% Run the function to generate values necessary and produce pretty graphs
[n, q, coolFactor] = F_627C(2, sphereInfoData, sphereInfoHeaders, sphereDataData, ...
    cylinderInfoData, cylinderInfoHeaders, cylinderDataData, table1Data, ...
    table1Headers, table2Data, table2Headers);

%% UI

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% General Structure for inputs (so I don't have to keep commenting it
% because it occurs so many times) [I would have restructured the code had
% this been done earlier in the project to make it more streamlined, but 
% time is not helpful here.]
%
%   input answer in dialog box
%   while it is an empty string or if the box is closed, tell the user and 
%   allow them to enter it again if they choose to or exit the program
%   convert the input (it comes in as a cell and that cannot be changed) to
%   a number if needed and store it in the correct variable
%
%   It's easier to reuse one variable because then I don't have to keep up
%   with so many names.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create a variable to enter the loop
back = 1;

while back == 1
    
   
    % Ask the user what they want to calculate
    whichWay = menu('Would you like to calculate cooling time or final temperature?', 'Cooling Time', 'Final Temperature');
    
    %% Cooling Time
    % Cooling time
    if whichWay == 1
        
        % Take in an answer using inputdlg and make sure the user didn't
        % just close the dialog box before checking if it's positive or not
        answer = (inputdlg('Enter the room temperature in degrees Celsius:'))
        
        % If the user closes the menu they see this in all cases
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the room temperature in degrees Celsius:'));
                
            end
        end
        
        % Finally store the variable
        rt = str2num(answer{1,1});
        
        % Undergo the same process if the value is not valid
        % While room temp is not within the correct range
        while rt > 100 || rt < 0
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            
            answer = (inputdlg('Enter the room temperature in degrees Celsius:'));
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the room temperature in degrees Celsius:'))
                    
                end
            end
            
            rt = str2num(answer{1,1});
            
        end
        
        answer = inputdlg('Enter the initial temperature:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the initial temperature in degrees Celsius:'))
                
            end
        end
        
        it = str2num(answer{1,1});
        
        
        
        answer = inputdlg('Enter the final temperature:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the room temperature in degrees Celsius:'))
                
            end
        end
        
        ft = str2num(answer{1,1});
        
        % check to make sure that the final heated temp is not greater than
        % the initial temp
        while ft > it
            h= msgbox('Your final temperature is greater than your initial temperature.', 'Error', 'warn');
            
            answer = inputdlg('Enter the initial temperature:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the initial temperature in degrees Celsius:'))
                    
                end
            end
            
            it = str2num(answer{1,1});
            
            
            
            answer = inputdlg('Enter the final temperature:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the final temperature in degrees Celsius:'))
                    
                end
            end
            ft = str2num(answer{1,1});
            
        end
        
     %% Final Temperature Choice
    elseif    whichWay == 2
        
        answer = inputdlg('Enter the cooling time in minutes:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the cooling time in minutes:'))
                
            end
        end
        
        ct = str2num(answer{1,1});
        
        % while the cooling time is not within a range
        while  ct > 30 || ct < 0
            h= msgbox('You entered a value outside of the valid range. Try again.', 'Error', 'warn');
            
            answer = inputdlg('Enter the cooling time in minutes:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the cooling time in minutes:'))
                    
                end
            end
            
            ct = str2num(answer{1,1});
            
        end
        
        
        answer = inputdlg('Enter the initial temperature:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the initial temperature in degrees Celsius:'))
                
            end
        end
        it = str2num(answer{1,1});
        
        
        
        answer = inputdlg('Enter the room temperature in degrees Celsius:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the room temperature in degrees Celsius:'))
                
            end
        end
        
        rt = str2num(answer{1,1});
        
        % While the values are not within a range
        while (rt > 100 || rt < 0) || (it > 100 || it < 0)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            
            answer = inputdlg('Enter the initial temperature:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the initial temperature in degrees Celsius:'))
                    
                end
            end
            
            it = str2num(answer{1,1});
            
            
            
            answer = inputdlg('Enter the room temperature in degrees Celsius:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the room temperature in degrees Celsius:'))
                    
                end
            end
            
            rt = str2num(answer{1,1});
            
        end
    %% User closes first menu    
    elseif whichWay==0
        exit = menu('Do you want to exit the program?','Yes','No');
        
        if exit == 1 || exit == 0
            return
        else back == 1;
        end
    end
    
    %% Choosing shapes
    shape = menu('Pick a shape from the options below:', 'Sphere', 'Cylinder', 'Rectangular Block');
    
    %% Spheres
    if shape == 1
        
        answer = inputdlg('Enter the radius of the sphere in inches:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the radius of the sphere in inches:'))
                
            end
        end
        
        radi = str2num(answer{1,1});
        
        
        A= 4*pi*(0.0254*radi)^2;
        V= 4/3*pi*(radi)^3;
        
        % while the radius is negative
        while radi <=0
            h= msgbox('Invalid value, please enter again', 'Error', 'warn');
            
            answer = inputdlg('Enter the radius of the sphere in inches:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the radius of the sphere in inches:'))
                    
                end
            end
            radi = str2num(answer{1,1});
            
            
            A= 4*pi*(0.0254*radi)^2;
            V= 4/3*pi*(radi)^3;
        end
     %% Cylinders   
    elseif shape == 2
        
        answer = inputdlg('Enter the radius of the cylinder in inches:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the radius of the cylinder in inches:'))
                
            end
        end
        radi = str2num(answer{1,1});
        
        
        
        answer = inputdlg('Enter the length of the cylinder in inches:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the length of the cylinder in inches:'))
                
            end
        end
        
        len = str2num(answer{1,1});
        
        
        A = 2*pi*(0.0254*radi)^2+2*pi*(0.0254*radi)*len;
        V=pi*radi^2*len;
        
        % while the diameter is 
        while radi <=0 || len <=0
            h= msgbox('Invalid value, please enter again', 'Error', 'warn');
            
            answer = inputdlg('Enter the radius of the cylinder in inches:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the radius of the cylinder in inches:'))
                    
                end
            end
            radi = str2num(answer{1,1});
            
            
            
            answer = inputdlg('Enter the length of the cylinder in inches:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the length of the cylinder in inches:'))
                    
                end
            end
            
            len = str2num(answer{1,1});
            
            
            V=pi*radi^2*h;
            A = 2*pi*(0.0254*radi)^2+2*pi*(0.0254*radi)*len;
            
        end
        
    %% Rectangle    
    elseif shape == 3
        
        answer = inputdlg('Enter the length of the rectangle in inches:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the length of the rectangle in inches:'))
                
            end
        end
        
        len = str2num(answer{1,1});
        
        
        
        answer = inputdlg('Enter the width of the rectangle in inches:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==1
                return
            else
                back == 1;
                answer = (inputdlg('Enter the width of the rectangle in inches:'))
                
            end
        end
        
        wid = str2num(answer{1,1});
        
        
        
        answer = inputdlg('Enter the thickness of the rectangle in inches:');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the thickness of the rectangle in inches:'))
                
            end
        end
        
        thk = str2num(answer{1,1});
        
        
        A = 2*((0.0254*len)*(0.0254*wid)+(0.0254*len)*(0.0254*thk)+(0.0254*wid)*(0.0254*thk));
        V=(0.0254*len)*(0.0254*thk)*(0.0254*wid);
        
        % while the values are not positive
        while len<=0 || wid<=0 || thk <=0
            h= msgbox('Invalid value, please enter again.', 'Error', 'warn')
            
            answer = inputdlg('Enter the length of the rectangle in inches:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==1
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the length of the rectangle in inches:'))
                    
                end
            end
            
            len = str2num(answer{1,1});
            
            
            
            answer = inputdlg('Enter the width of the rectangle in inches:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the width of the rectangle in inches:'))
                    
                end
            end
            
            wid = str2num(answer{1,1});
            
            
            
            answer = inputdlg('Enter the thickness of the rectangle in inches:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the thickness of the rectangle in inches:'))
                    
                end
            end
            
            thk = str2num(answer{1,1});
            
            
            A = 2*((0.0254*len)*(0.0254*wid)+(0.0254*len)*(0.0254*thk)+(0.0254*wid)*(0.0254*thk));
            V=(0.0254*len)*(0.0254*thk)*(0.0254*wid);
        end
        
     %% closes menu   
    else
        exit = menu('Do you want to exit the program?','Yes','No');
        
        if exit == 1 || exit == 0
            return
        else
            back == 1;
        end
        
    end
    
    %% Custom Material
    
    Mtr = menu('Choose a material',table1Headers{2:end,1},'Others');
    
    if Mtr == 8
        
        answer = inputdlg('Enter the name of the material: \n','s');
        
        while isempty(answer)
            h= msgbox('You put in a value that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the name of the material:'))
                
            end
        end
        
        MtrName = (answer{1,1});
        
        
        
        answer = inputdlg('Enter the specific gravity of the material: \n');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the specific gravity of the material:'))
                
            end
        end
        
        MtrSG = str2num(answer{1,1});
        
        
        
        answer = inputdlg('Enter the specific heat of the material: \n');
        
        while isempty(answer)
            h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
            exit = menu('Do you want to exit the program?','Yes','No');
            
            if exit == 1||exit==0
                return
            else
                back == 1;
                answer = (inputdlg('Enter the specific heat of the material:'))
                
            end
        end
        
        MtrSH = str2num(answer{1,1});
        
        
        while MtrSG <= 0 || MtrSH <= 0
            h= msgbox('Please enter positive value.', 'Error', 'warn');
            
            answer = inputdlg('Enter the name of the material:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the name of the material:'))
                    
                end
            end
            
            Mtr = answer{1,1};
            
            
            
            answer = inputdlg('Enter the specific gravity of the material:');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the specific gravity of the material:'))
                    
                end
            end
            
            MtrSG = str2num(answer{1,1});
            
            
            
            answer = inputdlg('Enter the specific heat of the material: \n');
            
            while isempty(answer)
                h= msgbox('You put in a number that is not in the valid range. Try again.', 'Error', 'warn');
                exit = menu('Do you want to exit the program?','Yes','No');
                
                if exit == 1||exit==0
                    return
                else
                    back == 1;
                    answer = (inputdlg('Enter the specific heat of the material:'))
                    
                end
            end
            
            MtrSH = str2num(answer{1,1});
            
            
            mass=MtrSG*V;
        end
        
        
        
        
    elseif Mtr == 0
        exit = menu('Do you want to exit the program?','Yes','No');
        
        if exit == 1||exit==0
            return
        else
            back == 1;
        end
        
    %% Find the correct material data    
    else
        findMe = find(strcmp(table1Headers(:,1), table1Headers{Mtr,1}));
        MtrSG = table1Data(findMe, 1);
        MtrSH = table1Data(findMe, 2);
        mass=MtrSG*V
        
        
    end
    
    
    %% Calculate the desired value
    
    r = A^n * (mass*MtrSH)^q * coolFactor;
    if whichWay == 1
        %Cooling time
        inside = (it-ft)/(it-rt);
        DesiredVal = -log(inside)/r;
        
        switch shape
            case 1
                outputDialog = sprintf('For a sphere made of %s with a diameter of %0.3f [in], the cooling factor is %0.4f [1/min].With a room temperature of %0.1f [%c C], cooling by natural convection from an initial temperature of %0.1f [%c C] to a final temperature of %0.1f [%c C] is predicted to take %0.1f [min].', table1Headers{Mtr+1,1}, radi*2, r, rt, char(176), it, char(176), ft, char(176), DesiredVal);
                
            case 2
                outputDialog = sprintf('For a cylinder made of %s with a diameter of %0.3f [in] and a length of %0.3f [in], the cooling factor is %0.4f [1/min]. With a room temperature of %0.1f [%c C], cooling by natural convection from an initial temperature of %0.1f [%c C] to a final temperature of %0.1f [%c C] is predicted to take %0.1f [min].', table1Headers{Mtr+1,1}, radi*2, len, r, rt, char(176), it, char(176), ft, char(176), DesiredVal);
            case 3
                outputDialog = sprintf('For a rectangular block made of %s with a length of %0.3f [in] a width of %0.3f [in] and a height of %0.3f [in], the cooling factor is %0.4f [1/min]. With a room temperature of %0.1f [%c C], cooling by natural convection from an initial temperature of %0.1f [%c C] to a final temperature of %0.1f [%c C] is predicted to take %0.1f [min].', table1Headers{Mtr+1,1}, radi*2, r, rt, char(176), it, char(176), ft, char(176), DesiredVal);
            otherwise
                outputDialog = sprintf('An error occurred');
        end
        
    else
        %Final Temp
        DesiredVal = (it-rt) * exp(-r*ct);
        
        switch shape
            case 1
                outputDialog = sprintf('For a sphere made of %s with a diameter of %0.3f [in], the cooling factor is %0.4f [1/min]. With a room temperature of %0.1f [%c C], after cooling for %0.1f minutes by natural convection from an initial temperature of %0.1f [%c C], the final temperature is predicted to be %0.1f [%c C].', table1Headers{Mtr+1,1}, radi*2, r, rt, char(176), ct, char(176), it, char(176), DesiredVal, char(176));
                
            case 2
                outputDialog = sprintf('For a cylinder made of %s with a diameter of %0.3f [in] and a length of %0.3f [in], the cooling factor is %0.4f [1/min]. With a room temperature of %0.1f [%c C], after cooling for %0.1f minutes by natural convection from an initial temperature of %0.1f [%c C], the final temperature is predicted to be %0.1f [%c C].', table1Headers{Mtr+1,1}, radi*2, r, rt, char(176), ct, char(176), it, char(176), DesiredVal, char(176));
                
            case 3
                outputDialog = sprintf('For a rectangular block made of %s with a length of %0.3f [in] a width of %0.3f [in] and a height of %0.3f [in], the cooling factor is %0.4f [1/min]. With a room temperature of %0.1f [%c C], after cooling for %0.1f minutes by natural convection from an initial temperature of %0.1f [%c C], the final temperature is predicted to be %0.1f [%c C].', table1Headers{Mtr+1,1}, radi*2, r, rt, char(176), ct, char(176), it, char(176), DesiredVal, char(176));
            otherwise
                outputDialog = sprintf('An error occurred.');
        end
        
        
    end
    
    
    h = msgbox(outputDialog);
    
    %% Continue the loop
    again=menu('Thank you for using, would you like to start over?','Yes','No');
    
    if again == 2||again == 0
        h = msgbox('Thank you for using')
        return
    else
        back =1;
    end
    
end